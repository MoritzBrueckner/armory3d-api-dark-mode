# Armory 3D API - Dark Mode

[![Install directly with Stylus](
https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad?style=for-the-badge)](https://gitlab.com/MoritzBrueckner/armory3d-api-dark-mode/raw/master/armory3d-api.user.css)

Dark mode for the API of [Armory3D](https://armory3d.org) (based on it's main page)  
Link to the API: https://armory3d.org/api/

## Installation

- Install [Stylus](https://add0n.com/stylus.html)

- Install the userstyle by clicking on the badge or [this link](https://gitlab.com/MoritzBrueckner/armory3d-api-dark-mode/raw/master/armory3d-api.user.css)

You can change the accent color (default is the background color of the Armory3D logo `#CF2B43`) by clicking on the cogwheel icon next to this style in the style overview of the Stylus extension.

## Screenshots

![Preview](./screenshots/armorydark.png "Preview")

![Static Methods](./screenshots/armorydark2.png "Static Methods")

![Parameter Details](./screenshots/armorydark3.png "Parameter Details")
